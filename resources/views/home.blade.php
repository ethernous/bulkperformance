@extends('layouts.app')

@section('content')
@if ($akses == 'Sales')
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> Last Time Sales Data Has Been
                    Imported</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'Rev')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'TR')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'Capex')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'all')
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> Last Time General Data Has Been
                    Imported</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="genCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Tenant Data
                                                </p>
                                                <h5 class="font-weight-bolder mb-0">
                                                    {{ \Carbon\Carbon::parse($tenant['updated_at'])->format('d M Y, H:m:s'); }}
                                                </h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tenant['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Area Data
                                                </p>
                                                <h5 class="font-weight-bolder mb-0">
                                                    {{ \Carbon\Carbon::parse($area['updated_at'])->format('d M Y, H:m:s'); }}
                                                </h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($area['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">SoW Data</p>
                                                <h5 class="font-weight-bolder mb-0">
                                                    {{ \Carbon\Carbon::parse($sow['updated_at'])->format('d M Y, H:m:s'); }}
                                                </h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($sow['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10"
                                                    aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> Last Time Sales Data Has Been
                    Imported</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="collapseCardExample">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2022 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2022['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2022['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2021 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2021['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2021['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($datasales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($datasales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Result Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($hassales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($hassales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="numbers">
                                                <p class="text-sm mb-0 text-capitalize font-weight-bold">2020 Sales Target Data</p>
                                                <h5 class="font-weight-bolder mb-0">{{ \Carbon\Carbon::parse($tarsales2020['updated_at'])->format('d M Y, H:m:s'); }}</h5>
                                                <p class="text-sm font-weight-bolder">{{ \Carbon\Carbon::parse($tarsales2020['updated_at'])->diffForHumans(\Carbon\Carbon::now()) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4 text-end">
                                            <div
                                                class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                                                <i class="ni ni-time-alarm text-lg opacity-10" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@push('scripts')
<script>
Swal.fire({
icon: 'success',
title: 'Welcome',
text: 'Welcome to Bulk Import Dashboard {{ Auth::user()->name }}'
})
</script>
@endpush
@endsection
