@extends('layouts.app')
@section('content')

@if ($akses == 'Sales')
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2022 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="genCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genaCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genaCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2021 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="genaCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2021</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genbCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genbCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2020 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="genbCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2020 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2020 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#gencCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="gencCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2019 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="gencCard">
                <div class="card-body">
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2019" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2019</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2019 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'Rev')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'TR')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'Capex')
<div class="row mt-2">
    <div class="col-xl-12 mt-4">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="text-center">
                    <h1>Under Construction</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif($akses == 'all')
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2022 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="genCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genaCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genaCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2021 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="genaCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2021</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#genbCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="genbCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2020 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="genbCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarsale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Target 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Target 2020 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Tenant</th>
                                                <th>Tenant 1</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2020 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datasale2020" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Data 2020</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Data 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Kategori Sales</th>
                                                <th>Site Name</th>
                                                <th>Longitude</th>
                                                <th>Latitude</th>
                                                <th>Bulan Accrue</th>
                                                <th>Ach Sales</th>
                                                <th>Tenant</th>
                                                <th>Area</th>
                                                <th>SoW 2</th>
                                                <th>Tanggal WO</th>
                                                <th>Status Const</th>
                                                <th>Sub Status Const</th>
                                                <th>Detail Status</th>
                                                <th>Date RFI</th>
                                                <th>BAUF</th>
                                                <th>BAPS</th>
                                                <th>Milestone Pivot</th>
                                                <th>Cek PO RFI</th>
                                                <th>General Blocking</th>
                                                <th>Isu Aging Wo to OG</th>
                                                <th>Drop Issue</th>
                                                <th>Batch Product</th>
                                                <th>Notes</th>
                                                <th>Block OG</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#gencCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="gencCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> 2019 Sales Data</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="gencCard">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hassale2019" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Sales Achievement Pivot 2019</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Sales Achievement Pivot 2019 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Des</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#gendCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="gendCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> Revenue Data 2022</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="gendCard">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datarev2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue 2022 Data Truncate</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Status Revenue</th>
                                                <th>Bulan Accrue</th>
                                                <th>Site Name</th>
                                                <th>Regional</th>
                                                <th>Area</th>
                                                <th>Segmen</th>
                                                <th>In/External</th>
                                                <th>Tenant WO</th>
                                                <th>Tenant</th>
                                                <th>SoW</th>
                                                <th>Porto 1</th>
                                                <th>Porto 2</th>
                                                <th>Detail Produk</th>
                                                <th>Start Sewa</th>
                                                <th>End Sewa</th>
                                                <th>PPM</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datarev20222" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue 2022 Data</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue 2022 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Status Revenue</th>
                                                <th>Bulan Accrue</th>
                                                <th>Site Name</th>
                                                <th>Regional</th>
                                                <th>Area</th>
                                                <th>Segmen</th>
                                                <th>In/External</th>
                                                <th>Tenant WO</th>
                                                <th>Tenant</th>
                                                <th>SoW</th>
                                                <th>Porto 1</th>
                                                <th>Porto 2</th>
                                                <th>Detail Produk</th>
                                                <th>Start Sewa</th>
                                                <th>End Sewa</th>
                                                <th>PPM</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hasrev2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue Achievement Data 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue Achievement Data 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Portofolio</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tarrev2022" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue Target 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue Target 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Portofolio</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hasrev2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue Achievement Data 2021</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue Achievement Data 2021</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Portofolio</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datarev2021" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue 2021 Data Truncate</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Status Revenue</th>
                                                <th>Bulan Accrue</th>
                                                <th>Site Name</th>
                                                <th>Regional</th>
                                                <th>Area</th>
                                                <th>Segmen</th>
                                                <th>In/External</th>
                                                <th>Tenant WO</th>
                                                <th>Tenant</th>
                                                <th>SoW</th>
                                                <th>Porto 1</th>
                                                <th>Porto 2</th>
                                                <th>Detail Produk</th>
                                                <th>Start Sewa</th>
                                                <th>End Sewa</th>
                                                <th>PPM</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datarev20212" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">Revenue 2021 Data</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import Revenue 2021 Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID Akhir</th>
                                                <th>Status Revenue</th>
                                                <th>Bulan Accrue</th>
                                                <th>Site Name</th>
                                                <th>Regional</th>
                                                <th>Area</th>
                                                <th>Segmen</th>
                                                <th>In/External</th>
                                                <th>Tenant WO</th>
                                                <th>Tenant</th>
                                                <th>SoW</th>
                                                <th>Porto 1</th>
                                                <th>Porto 2</th>
                                                <th>Detail Produk</th>
                                                <th>Start Sewa</th>
                                                <th>End Sewa</th>
                                                <th>PPM</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-2">
    <div class="col-xl-12 col-md-12 col-mb-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#geneCard" class="d-block card-header py-3" data-bs-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="geneCard">
                <h6 class="m-0 font-weight-bold text-dark"><i class="fas fa-clock"></i> TR Data 2022</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse" id="geneCard">
                <div class="card-body">
                    <div class="row ">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datatr" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">TR Data Truncate</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import TR Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>PID</th>
                                                <th>Site ID DMT</th>
                                                <th>Site Name</th>
                                                <th>Tenant</th>
                                                <th>Kat Tenant</th>
                                                <th>Jumlah Tenant</th>
                                                <th>Jumlah Tower</th>
                                                <th>Bulan Alpro</th>
                                                <th>Tahun Alpro</th>
                                                <th>Kategori</th>
                                                <th>SoW</th>
                                                <th>Tipe Site</th>
                                                <th>Tipe Tower</th>
                                                <th>Kat Ketinggian</th>
                                                <th>Area</th>
                                                <th>Demografi</th>
                                                <th>Market</th>
                                                <th>Fibre</th>
                                                <th>Kat Tower</th>
                                                <th>Tower Owner</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/datatr2" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">TR Data</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import TR Data</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>Portofolio</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/tartr" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">TR Target 2022</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import TR Target 2022</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hasiltrall" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">TR All</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import TR All</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-xl-12 col-sm-12 mb-xl-0 mb-4 mt-4">
                            <div class="card bg-secondary">
                                <div class="card-body p-3">
                                    <form action="/hasiltrnew" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label class="text-white font-weight-bold">TR New</label>
                                        <input type="file" name="file" class="form-control">
                                        <br>
                                        <button class="btn btn-danger">Import TR New</button>
                                    </form>
                                    <div class="text-xs font-weight-bold text-white">
                                        Make sure your .xlsx file contains the data in this following order
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-dark table-hover">
                                            <thead class="text-xs">
                                                <th>No</th>
                                                <th>SoW</th>
                                                <th>Jan</th>
                                                <th>Feb</th>
                                                <th>Mar</th>
                                                <th>Apr</th>
                                                <th>May</th>
                                                <th>Jun</th>
                                                <th>Jul</th>
                                                <th>Aug</th>
                                                <th>Sep</th>
                                                <th>Oct</th>
                                                <th>Nov</th>
                                                <th>Dec</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@push('scripts')
@if (\Session::has('success'))
<script>
  Swal.fire({
  icon: 'success',
  title: 'Success',
  text: '{!! \Session::get('success') !!}'
})
</script>
@endif
@endpush
@endsection