<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datatr', function (Blueprint $table) {
            $table->id('id_datatr');
            $table->string('siteiddmt');
            $table->string('sitename');
            $table->string('pid');
            $table->string('tenant');
            $table->string('kattenant');
            $table->string('jumtenant');
            $table->string('jumtower');
            $table->string('bulan');
            $table->string('tahun');
            $table->string('netadd');
            $table->string('sow');
            $table->string('tipesite');
            $table->string('tipetower');
            $table->string('ketinggian');
            $table->string('area');
            $table->string('demografi');
            $table->string('market');
            $table->string('fiber');
            $table->string('kattower');
            $table->string('toowner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datatr');
    }
};
