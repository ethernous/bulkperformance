<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasiltrall', function (Blueprint $table) {
            $table->id('id_hasiltr');
            $table->string('sow');
            $table->string('2009');
            $table->string('2010');
            $table->string('2011');
            $table->string('2012');
            $table->string('2013');
            $table->string('2014');
            $table->string('2015');
            $table->string('2016');
            $table->string('2017');
            $table->string('2018');
            $table->string('2019');
            $table->string('2020');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasiltrall');
    }
};
