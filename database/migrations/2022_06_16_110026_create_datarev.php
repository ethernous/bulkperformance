<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datarev', function (Blueprint $table) {
            $table->id('id_datarev');
            $table->string('piddmt')->nullable();
            $table->string('kategori')->nullable();
            $table->string('bulan')->nullable();
            $table->string('sitename')->nullable();
            $table->string('regional')->nullable();
            $table->string('area')->nullable();
            $table->string('segmen')->nullable();
            $table->string('inteks')->nullable();
            $table->string('tenantwo')->nullable();
            $table->string('tenant')->nullable();
            $table->string('sow')->nullable();
            $table->string('portofolio1')->nullable();
            $table->string('portofolio2')->nullable();
            $table->string('detproduk')->nullable();
            $table->string('startsewa')->nullable();
            $table->string('endsewa')->nullable();
            $table->string('ppm')->nullable();
            $table->string('jan')->nullable();
            $table->string('feb')->nullable();
            $table->string('mar')->nullable();
            $table->string('apr')->nullable();
            $table->string('may')->nullable();
            $table->string('jun')->nullable();
            $table->string('jul')->nullable();
            $table->string('aug')->nullable();
            $table->string('sep')->nullable();
            $table->string('oct')->nullable();
            $table->string('nov')->nullable();
            $table->string('dec')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datarev');
    }
};
