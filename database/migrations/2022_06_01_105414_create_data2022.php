<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datasale2022', function (Blueprint $table) {
            $table->id('id_data2022');
            $table->string('piddmt')->nullable();
            $table->string('kategori')->nullable();
            $table->string('sitename')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('bulan')->nullable();
            $table->string('achsales')->nullable();
            $table->string('tenant')->nullable();
            $table->string('area')->nullable();
            $table->string('sow')->nullable();
            $table->string('tanggalwo')->nullable();
            $table->string('status')->nullable();
            $table->string('substatus')->nullable();
            $table->string('detstatus')->nullable();
            $table->string('daterfi')->nullable();
            $table->string('bauf')->nullable();
            $table->string('baps')->nullable();
            $table->string('milestone')->nullable();
            $table->string('cekporfi')->nullable();
            $table->string('general')->nullable();
            $table->string('isuwoog')->nullable();
            $table->string('dropisu')->nullable();
            $table->string('batch')->nullable();
            $table->string('notes')->nullable();
            $table->string('blockog')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data2022');
    }
};
