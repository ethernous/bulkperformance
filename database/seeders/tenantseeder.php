<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tenant;

class tenantseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = [[
            'namatenant'=>'TSEL',
            'deskripsi'=>'Telkomsel',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'H3I',
            'deskripsi'=>'Hutchinson 3',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'ISAT',
            'deskripsi'=>'Indosat',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'XL',
            'deskripsi'=>'XL',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'TELKOM',
            'deskripsi'=>'PT. Telkom',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'SMART',
            'deskripsi'=>'Smartfren',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'Others',
            'deskripsi'=>'Others',
            'tahun'=>'2022',
        ],[
            'namatenant'=>'TSEL',
            'deskripsi'=>'Telkomsel',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'H3I',
            'deskripsi'=>'Hutchinson 3',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'ISAT',
            'deskripsi'=>'Indosat',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'XL',
            'deskripsi'=>'XL',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'TELKOM',
            'deskripsi'=>'PT. Telkom',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'SMART',
            'deskripsi'=>'Smartfren',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'STI',
            'deskripsi'=>'STI',
            'tahun'=>'2021',
        ],[
            'namatenant'=>'Others',
            'deskripsi'=>'Others',
            'tahun'=>'2021',
        ],];
        foreach ($tenant as $key => $value) {
            Tenant::create($value);
        }
    }
}
