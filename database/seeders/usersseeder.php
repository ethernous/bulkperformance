<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class usersseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
               'name'=>'Ilham Majid Rabbani',
               'email'=>'ilhammajid123@gmail.com',
               'password'=> bcrypt('mitratel'),
               'jabatan'=>'Admin MSA',
               'jobdesc'=>'all',
            ],
            [
                'name'=>'Andrian Sulistiawan',
                'email'=>'andrian.sulistiawan@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Manager MPA',
                'jobdesc'=>'all',
            ],
            [
                'name'=>'Enang Gunawan',
                'email'=>'enang.gunawan@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MPA',
                'jobdesc'=>'TR',
            ],
            [
                'name'=>'Chandraningtyas Utami',
                'email'=>'chandraningtyas.utami@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MPA',
                'jobdesc'=>'TR',
            ],
            [
                'name'=>'Ahmad Mahfud',
                'email'=>'ahmad.mahfud@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MPA',
                'jobdesc'=>'TR',
            ],
            [
                'name'=>'Fista Monica',
                'email'=>'fista.monica@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MSA',
                'jobdesc'=>'Capex',
            ],
            [
                'name'=>'Harry Firmanzah',
                'email'=>'harry@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Manager MSA',
                'jobdesc'=>'all',
            ],
            [
                'name'=>'Bety Puspitasari',
                'email'=>'bety.puspitasari@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MSA',
                'jobdesc'=>'Rev',
            ],
            [
                'name'=>'Yudhistira Angga',
                'email'=>'yudhistira.angga@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Officer MSA',
                'jobdesc'=>'Capex',
            ],
            [
                'name'=>'Ganang Arifian',
                'email'=>'ganang.arifian@pt-sdm.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Admin MSA',
                'jobdesc'=>'Sales',
            ],
            [
                'name'=>'Alex Yapis Iskandar',
                'email'=>'alex.iskandar@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'GM MSA',
                'jobdesc'=>'all',
            ],
            [
                'name'=>'Haris Ali',
                'email'=>'haris.maruf914@gmail.com',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'Admin MSA',
                'jobdesc'=>'all',
            ],
            [
                'name'=>'Elfri Jufri',
                'email'=>'elfri.jufri@mitratel.co.id',
                'password'=> bcrypt('mitratel'),
                'jabatan'=>'EGM MSA',
                'jobdesc'=>'all',
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }

    }
}
