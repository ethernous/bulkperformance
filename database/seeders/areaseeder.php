<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Area;

class areaseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = [[
            'namaarea'=>'Area 1',
            'deskripsi'=>'Meliputi Pulau Sumatera',
        ],[
            'namaarea'=>'Area 2',
            'deskripsi'=>'Meliputi Pulau Jawa',
        ],[
            'namaarea'=>'Area 3',
            'deskripsi'=>'Meliputi Pulau Bali dan Nusa Tenggara',
        ],[
            'namaarea'=>'Area 4',
            'deskripsi'=>'Meliputi Pulau Kalimantan, Sulawesi, Maluku dan Papua',
        ],];
        foreach ($area as $key => $value) {
            Area::create($value);
        }
    }
}
