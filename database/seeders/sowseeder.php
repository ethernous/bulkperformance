<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Sow;

class sowseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sow = [[
            'jenissow'=>'Colo',
            'tahunsow'=>'2022',
            'deskripsi'=>'Colo',
        ],[
            'jenissow'=>'New Site',
            'tahunsow'=>'2022',
            'deskripsi'=>'New Site',
        ],[
            'jenissow'=>'Reseller',
            'tahunsow'=>'2022',
            'deskripsi'=>'Reseller',
        ],[
            'jenissow'=>'Macro',
            'tahunsow'=>'2021',
            'deskripsi'=>'Macro',
        ],[
            'jenissow'=>'Micro',
            'tahunsow'=>'2021',
            'deskripsi'=>'Micro',
        ],[
            'jenissow'=>'Colo',
            'tahunsow'=>'2021',
            'deskripsi'=>'Colo',
        ],[
            'jenissow'=>'Reseller',
            'tahunsow'=>'2021',
            'deskripsi'=>'Reseller',
        ],[
            'jenissow'=>'Macro',
            'tahunsow'=>'2020',
            'deskripsi'=>'Macro',
        ],[
            'jenissow'=>'Micro',
            'tahunsow'=>'2020',
            'deskripsi'=>'Micro',
        ],[
            'jenissow'=>'Colo',
            'tahunsow'=>'2020',
            'deskripsi'=>'Colo',
        ],[
            'jenissow'=>'Reseller',
            'tahunsow'=>'2020',
            'deskripsi'=>'Reseller',
        ],];
        foreach ($sow as $key => $value) {
            Sow::create($value);
        }
    }
}
