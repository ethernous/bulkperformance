<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register'=>false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('Dashboard');
Route::get('/import', [App\Http\Controllers\ImportController::class, 'index'])->name('Import');
Route::get('/export', [App\Http\Controllers\ExportController::class, 'index'])->name('Export');

// Sales Import Routes
Route::post('/tarsale2022', [App\Http\Controllers\ImportController::class, 'tarsale2022'])->name('tarsale2022');
Route::post('/hassale2022', [App\Http\Controllers\ImportController::class, 'hassale2022'])->name('hassale2022');
Route::post('/datasale2022', [App\Http\Controllers\ImportController::class, 'datasale2022'])->name('datasale2022');

Route::post('/tarsale2021', [App\Http\Controllers\ImportController::class, 'tarsale2021'])->name('tarsale2021');
Route::post('/hassale2021', [App\Http\Controllers\ImportController::class, 'hassale2021'])->name('hassale2021');
Route::post('/datasale2021', [App\Http\Controllers\ImportController::class, 'datasale2021'])->name('datasale2021');

Route::post('/tarsale2020', [App\Http\Controllers\ImportController::class, 'tarsale2020'])->name('tarsale2020');
Route::post('/hassale2020', [App\Http\Controllers\ImportController::class, 'hassale2020'])->name('hassale2020');
Route::post('/datasale2020', [App\Http\Controllers\ImportController::class, 'datasale2020'])->name('datasale2020');

Route::post('/hassale2019', [App\Http\Controllers\ImportController::class, 'hassale2019'])->name('hassale2019');
// End Sales Import
//Revenue Import
Route::post('/datarev2022', [App\Http\Controllers\ImportController::class, 'datarev2022'])->name('datarev2022');
Route::post('/datarev20222', [App\Http\Controllers\ImportController::class, 'datarev2022'])->name('datarev20222');
Route::post('/datarev2021', [App\Http\Controllers\ImportController::class, 'datarev2021'])->name('datarev2021');
Route::post('/datarev20212', [App\Http\Controllers\ImportController::class, 'datarev20212'])->name('datarev20212');
Route::post('/hasrev2022', [App\Http\Controllers\ImportController::class, 'hasrev2022'])->name('hasrev2022');
Route::post('/tarrev2022', [App\Http\Controllers\ImportController::class, 'tarrev2022'])->name('tarrev2022');
Route::post('/hasrev2021', [App\Http\Controllers\ImportController::class, 'hasrev2021'])->name('hasrev2021');
//End Revenue Imports
//TR Import
Route::post('/datatr', [App\Http\Controllers\ImportController::class, 'datatr'])->name('datatr');
Route::post('/datatr2', [App\Http\Controllers\ImportController::class, 'datatr2'])->name('datatr2');
Route::post('/tartr', [App\Http\Controllers\ImportController::class, 'tartr'])->name('tartr');
Route::post('/hasiltrall', [App\Http\Controllers\ImportController::class, 'hasiltrall'])->name('hasiltrall');
Route::post('/hasiltrnew', [App\Http\Controllers\ImportController::class, 'hasiltrnew'])->name('hasiltrnew');