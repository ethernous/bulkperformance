<?php
  
namespace App\Imports;
  
use App\Models\Datatr;
use Maatwebsite\Excel\Concerns\ToModel;
  
class datatr2022_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Datatr([
            'id_datatr'       => $row[0],
            'siteiddmt'        => $row[1],
            'sitename'        => $row[2],
            'pid'           => $row[3],
            'tenant'           => $row[4],
            'kattenant'           => $row[5],
            'jumtenant'           => $row[6],
            'jumtower'           => $row[7],
            'bulan'           => $row[8],
            'tahun'           => $row[9],
            'netadd'           => $row[10],
            'sow'               => $row[11],
            'tipesite'           => $row[12],
            'tipetower'           => $row[13],
            'ketinggian'           => $row[14],
            'area'           => $row[15],
            'demografi'           => $row[16],
            'market'           => $row[17],
            'fiber'           => $row[18],
            'kattower'           => $row[19],
            'toowner'           => $row[20],
        ]);
    }
}