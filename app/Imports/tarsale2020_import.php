<?php
  
namespace App\Imports;
  
use App\Models\Tarsales2020;
use Maatwebsite\Excel\Concerns\ToModel;
  
class tarsale2020_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Tarsales2020([
            'id_tar2020'       => $row[0],
            'sow'           => $row[1],
            'jan'           => $row[2],
            'feb'           => $row[3],
            'mar'           => $row[4],
            'apr'           => $row[5],
            'may'           => $row[6],
            'jun'           => $row[7],
            'jul'           => $row[8],
            'aug'           => $row[9],
            'sep'           => $row[10],
            'oct'           => $row[11],
            'nov'           => $row[12],
            'dec'           => $row[13],
        ]);
    }
}