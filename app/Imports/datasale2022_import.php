<?php
  
namespace App\Imports;
  
use App\Models\Datasales2022;
use Maatwebsite\Excel\Concerns\ToModel;
  
class datasale2022_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Datasales2022([
            'id_data2022'   => $row[0],
            'piddmt'        => $row[1],
            'kategori'      => $row[2],
            'sitename'      => $row[3],
            'longitude'     => $row[4],
            'latitude'      => $row[5],
            'bulan'         => $row[6],
            'achsales'      => $row[7],
            'tenant'        => $row[8],
            'area'          => $row[9],
            'sow'           => $row[10],
            'tanggalwo'     => $row[11],
            'status'        => $row[12],
            'substatus'     => $row[13],
            'detstatus'     => $row[14],
            'daterfi'       => $row[15],
            'bauf'          => $row[16],
            'baps'          => $row[17],
            'milestone'     => $row[18],
            'cekporfi'      => $row[19],
            'general'       => $row[20],
            'isuwoog'       => $row[21],
            'dropisu'       => $row[22],
            'batch'         => $row[23],
            'notes'         => $row[24],
            'blockog'       => $row[25],
        ]);
    }
}