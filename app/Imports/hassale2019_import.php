<?php
  
namespace App\Imports;
  
use App\Models\Hassales2019;
use Maatwebsite\Excel\Concerns\ToModel;
  
class hassale2019_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Hassales2019([
            'id_has2019'       => $row[0],
            'sow'        => $row[1],
            '1'        => $row[2],
            '2'           => $row[3],
            '3'           => $row[4],
            '4'           => $row[5],
            '5'           => $row[6],
            '6'           => $row[7],
            '7'           => $row[8],
            '8'           => $row[9],
            '9'           => $row[10],
            '10'           => $row[11],
            '11'           => $row[12],
            '12'           => $row[13],
        ]);
    }
}