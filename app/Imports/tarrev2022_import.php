<?php
  
namespace App\Imports;
  
use App\Models\Tarrev;
use Maatwebsite\Excel\Concerns\ToModel;
  
class tarrev2022_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Tarrev([
            'id_tarrev'   => $row[0],
            'portofolio1'        => $row[1],
            'portofolio2'      => $row[2],
            'jan'     => $row[3],
            'feb'      => $row[4],
            'mar'       => $row[5],
            'apr'       => $row[6],
            'may'       => $row[7],
            'jun'         => $row[8],
            'jul'         => $row[9],
            'aug'       => $row[10],
            'sep'       => $row[11],
            'oct'       => $row[12],
            'nov'       => $row[13],
            'dec'       => $row[14],
        ]);
    }
}