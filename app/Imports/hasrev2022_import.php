<?php
  
namespace App\Imports;
  
use App\Models\Hasrev;
use Maatwebsite\Excel\Concerns\ToModel;
  
class hasrev2022_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Hasrev([
            'id_hasrev'   => $row[0],
            'portofolio1'  => $row[1],
            'portofolio2'  => $row[2],
            '1'           => $row[3],
            '2'           => $row[4],
            '3'           => $row[5],
            '4'           => $row[6],
            '5'           => $row[7],
            '6'           => $row[8],
            '7'           => $row[9],
            '8'           => $row[10],
            '9'           => $row[11],
            '10'           => $row[12],
            '11'           => $row[13],
            '12'           => $row[14],
        ]);
    }
}