<?php
  
namespace App\Imports;
  
use App\Models\Datarev;
use Maatwebsite\Excel\Concerns\ToModel;
  
class datarev_import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Datarev([
            'id_datarev'   => $row[0],
            'piddmt'        => $row[1],
            'kategori'      => $row[2],
            'bulan'      => $row[3],
            'sitename'     => $row[4],
            'regional'      => $row[5],
            'area'         => $row[6],
            'segmen'      => $row[7],
            'inteks'        => $row[8],
            'tenantwo'          => $row[9],
            'tenant'           => $row[10],
            'sow'     => $row[11],
            'portofolio1'        => $row[12],
            'portofolio2'     => $row[13],
            'detproduk'     => $row[14],
            'startsewa'       => $row[15],
            'endsewa'          => $row[16],
            'ppm'          => $row[17],
            'jan'     => $row[18],
            'feb'      => $row[19],
            'mar'       => $row[20],
            'apr'       => $row[21],
            'may'       => $row[22],
            'jun'         => $row[23],
            'jul'         => $row[24],
            'aug'       => $row[25],
            'sep'       => $row[26],
            'oct'       => $row[27],
            'nov'       => $row[28],
            'dec'       => $row[29],
        ]);
    }
}