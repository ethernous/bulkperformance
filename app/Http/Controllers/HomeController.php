<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Area;use App\Models\Tenant;use App\Models\Sow;
use App\Models\Tarsales2020;use App\Models\Tarsales2021;use App\Models\Tarsales2022;
use App\Models\Hassales2019;use App\Models\Hassales2020;use App\Models\Hassales2021;use App\Models\Hassales2022;
use App\Models\Datasales2020;use App\Models\Datasales2021;use App\Models\Datasales2022;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $akses = Auth::User()->jobdesc;
        $tenant = Tenant::select('updated_at')->orderBy('updated_at', 'DESC')->first();
        $sow = Sow::select('updated_at')->orderBy('updated_at', 'DESC')->first();
        $area = Area::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $hassales2019 = Hassales2019::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $hassales2020 = Hassales2020::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $hassales2021 = Hassales2021::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $hassales2022 = Hassales2022::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $tarsales2020 = Tarsales2020::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $tarsales2021 = Tarsales2021::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $tarsales2022 = Tarsales2022::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $datasales2020 = Datasales2020::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $datasales2021 = Datasales2021::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        $datasales2022 = Datasales2022::select('updated_at')->OrderBy('updated_at', 'DESC')->first();
        return view('home',[
            'akses'=>$akses,
            'tenant'=>$tenant,
            'sow'=>$sow,
            'area'=>$area,
            'hassales2020'=>$hassales2020,
            'hassales2021'=>$hassales2021,
            'hassales2022'=>$hassales2022,
            'tarsales2020'=>$tarsales2020,
            'tarsales2021'=>$tarsales2021,
            'tarsales2022'=>$tarsales2022,
            'datasales2020'=>$datasales2020,
            'datasales2021'=>$datasales2021,
            'datasales2022'=>$datasales2022,
        ]);
    }
}
