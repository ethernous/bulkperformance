<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

use App\Models\Tarsales2022;use App\Models\Tarsales2021;use App\Models\Tarsales2020;
use App\Models\Hassales2022;use App\Models\Hassales2021;use App\Models\Hassales2020;use App\Models\Hassales2019;
use App\Models\Datasales2022;use App\Models\Datasales2021;use App\Models\Datasales2020;
use App\Models\Datarev;use App\Models\Hasrev;use App\Models\Tarrev;use App\Models\Datarev2021;use App\Models\Hasrev2021;
use App\Models\Datatr;use App\Models\Hasiltrall;use App\Models\Tartr2022;use App\Models\Hasiltrnew;

use App\Imports\tarsale2022_import;use App\Imports\tarsale2021_import;use App\Imports\tarsale2020_import;
use App\Imports\hassale2022_import;use App\Imports\hassale2021_import;use App\Imports\hassale2020_import;use App\Imports\hassale2019_import;
use App\Imports\datasale2022_import;use App\Imports\datasale2021_import;use App\Imports\datasale2020_import;
use App\Imports\datarev_import;use App\Imports\hasrev2022_import;use App\Imports\tarrev2022_import;
use App\Imports\datarev2021_import;use App\Imports\hasrev2021_import;
use App\Imports\datatr2022_import;use App\Imports\tartr2022_import;use App\Imports\hasiltrall_import;use App\Imports\hasiltrnew_import;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $akses = Auth::User()->jobdesc;
        return view('import',[
            'akses'=>$akses,
        ]);
    }

    public function tarsale2022(){
        Tarsales2022::truncate();
        Excel::import(new tarsale2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2022 Sales target has been imported!']);
    }
    public function tarsale2021(){
        Tarsales2021::truncate();
        Excel::import(new tarsale2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2021 Sales target has been imported!']);
    }
    public function tarsale2020(){
        Tarsales2020::truncate();
        Excel::import(new tarsale2020_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2020 Sales target has been imported!']);
    }
    public function hassale2022(){
        Hassales2022::truncate();
        Excel::import(new hassale2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2022 Sales result has been imported!']);
    }
    public function hassale2021(){
        Hassales2021::truncate();
        Excel::import(new hassale2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2021 Sales result has been imported!']);
    }
    public function hassale2020(){
        Hassales2020::truncate();
        Excel::import(new hassale2020_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2020 Sales result has been imported!']);
    }
    public function hassale2019(){
        Hassales2019::truncate();
        Excel::import(new hassale2019_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2019 Sales result has been imported!']);
    }
    public function datasale2022(){
        Datasales2022::truncate();
        Excel::import(new datasale2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2022 Sales data has been imported!']);
    }
    public function datasale2021(){
        Datasales2021::truncate();
        Excel::import(new datasale2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2021 Sales data has been imported!']);
    }
    public function datasale2020(){
        Datasales2020::truncate();
        Excel::import(new datasale2020_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of 2020 Sales data has been imported!']);
    }

    
    public function datarev2022(){
        Datarev::truncate();
        Excel::import(new datarev_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2022 data has been imported!']);
    }
    public function datarev20222(){
        Excel::import(new datarev_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2022 data has been imported!']);
    }
    public function hasrev2022(){
        Hasrev::truncate();
        // dd(request()->all());
        Excel::import(new hasrev2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2022 achievement has been imported!']);
    }
    public function tarrev2022(){
        Tarrev::truncate();
        Excel::import(new tarrev2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2022 target has been imported!']);
    }
    public function datarev2021(){
        Datarev2021::truncate();
        Excel::import(new datarev2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2021 data has been imported!']);
    }
    public function datarev20212(){
        Excel::import(new datarev2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2021 data has been imported!']);
    }
    public function hasrev2021(){
        Hasrev2021::truncate();
        Excel::import(new hasrev2021_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Revenue 2021 data has been imported!']);
    }

    public function datatr(){
        Datatr::truncate();
        Excel::import(new datatr2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Tenancy Ratio data has been imported!']);
    }
    public function datatr2(){
        Excel::import(new datatr2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Tenancy Ratio data has been imported!']);
    }
    public function tartr(){
        Tartr2022::truncate();
        Excel::import(new tartr2022_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Tenancy Ratio Target data has been imported!']);
    }
    public function hasiltrall(){
        Hasiltrall::truncate();
        Excel::import(new hasiltrall_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Tenancy Ratio Target data has been imported!']);
    }
    public function hasiltrnew(){
        Hasiltrnew::truncate();
        Excel::import(new hasiltrnew_import,request()->file('file'));
        return redirect()->back()->with(['success' => 'New Data of Tenancy Ratio Target data has been imported!']);
    }
}
