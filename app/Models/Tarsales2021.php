<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarsales2021 extends Model
{
    use HasFactory;
    protected $guarded = array();
    protected $table = 'tarsale2021';
}
