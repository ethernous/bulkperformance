<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datasales2022 extends Model
{
    use HasFactory;
    protected $guarded = array();
    protected $table = 'datasale2022';
}
