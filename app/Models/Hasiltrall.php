<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasiltrall extends Model
{
    use HasFactory;
    protected $guarded = array();
    protected $table = 'hasiltrall';
}
