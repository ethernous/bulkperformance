<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasrev extends Model
{
    use HasFactory;
    protected $guarded = array();
    protected $table = 'hasrev2022';
}
