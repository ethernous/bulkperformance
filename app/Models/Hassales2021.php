<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hassales2021 extends Model
{
    use HasFactory;
    protected $guarded = array();
    protected $table = 'hassale2021';
}
