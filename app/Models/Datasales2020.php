<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datasales2020 extends Model
{
    use HasFactory;
    
    protected $guarded = array();
    protected $table = 'datasale2020';
}
